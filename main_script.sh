## configure universal values
BDR_PATH="/entrepot/donnees/edna/referenceDatabase/EMBL_ecoPCR/"
RD_prefix="embl_std"
#FOLDER_CONFIGFILES="./config/"
FOLDER_CONFIGFILES=$1


## convert the embl database in the format ecopcr
if [ -f $embl_std ]
then
    echo "EMBL database has already been converted into ecoPCR format"
else
    obiconvert --skip-on-error --embl -t ${BDR_PATH}/TAXO --ecopcrdb-output="${RD_prefix}" ${BDR_PATH}/EMBL/rel_std_*.dat
fi
RD_prefix=${BDR_PATH}${RD_prefix}

## find the species amplified by all your pairs of primers identified
for i in $(ls ${FOLDER_CONFIGFILES}*.sh)
do
source $i
## ecoPCR to simulate an in silico PCR
ecoPCR -d "${RD_prefix}" -e "${ecoPCR_e}" -l "${ecoPCR_l}" -L "${ecoPCR_L}" "${primerF}" "${primerR}" > "${rd_prefix}".ecopcr
## filter sequences so that they have a good taxonomic description at the species, genus and family levels
obigrep -d "${RD_prefix}" --require-rank=species --require-rank=genus --require-rank=family "${rd_prefix}".ecopcr > "${rd_prefix}"_clean.fasta
## remove redundant sequences
obiuniq -d "${RD_prefix}" "${rd_prefix}"_clean.fasta > "${rd_prefix}"_clean_uniq.fasta
## ensure that the dereplicated sequences have a taxid at the family level
obigrep -d "${RD_prefix}" --require-rank=family "${rd_prefix}"_clean_uniq.fasta > "${rd_prefix}"_clean_uniq_clean.fasta
## ensure that sequences each have a unique identification
obiannotate --uniq-id "${rd_prefix}"_clean_uniq_clean.fasta > "${rd_prefix}".fasta
## your reference database is built !
cp "${rd_prefix}".fasta results
mv "${rd_prefix}".ecopcr /entrepot/tmp/
done


