## To generate config files for each primers
## 1) Specify folder where to generate the output config files
CONFIG_FOLDER="config/callinectes_sapidus/"
## 2) Specify where the CSV primers files are stored
## CSV primers files must be ; seperated
## CSV primers files have primer as row
## CSV primers files have the following column:
## geneName;Identifier;GenePosition;PrimerForward;PrimerReverse;StartingPositionReverse;LengthOfFragment
PRIMERS_FOLDER="config/raw/callinectes_sapidus/"
##
## USAGE
## simply run the bash command
## bash gen_config.sh

function gen_configs () {
    PRIMER_CSV=$1
    CONFIG_FOLDER=$2
    for ligne in `awk -F';' '{ print $1"_"$2";"$4";"$5}' ${PRIMER_CSV}`;
    do
    RD_PREFIX=`echo ${ligne} | awk -F';' '{ print $1}'`
    primerF=`echo ${ligne} | awk -F';' '{ print $2}'`
    primerR=`echo ${ligne} | awk -F';' '{ print $3}'`
    CONFIGFILENAME=${CONFIG_FOLDER}/${RD_PREFIX}".sh"
    cat config/pattern_config.sh > ${CONFIGFILENAME}
    echo "rd_prefix="${RD_PREFIX} >> ${CONFIGFILENAME}
    echo "primerF="${primerF} >> ${CONFIGFILENAME}
    echo "primerR="${primerR} >> ${CONFIGFILENAME}
    done
}


for PRIMERS_FILE in `ls ${PRIMERS_FOLDER}*`; do gen_configs "${PRIMERS_FILE}" "${CONFIG_FOLDER}";done

