## primers pair prefix
rd_prefix="primers_1"

## ecoPCR arguments
### [-e] Maximum number of errors (mismatches) allowed per primer.
ecoPCR_e=3
### [-l] Minimum length of the in silico amplified DNA fragment, excluding primers.
ecoPCR_l=20
### [-L] Maximum length of the in silico amplified DNA fragment, excluding primers.
ecoPCR_L=1000
### 5' primer sequence (Forward)
primerF="AGTTTCTGACTTTTGCCTCCTTCC"
### 3' primer sequence (Reverse)
primerR="GTGAGCTAAATTTCCTGCAAGAGG"